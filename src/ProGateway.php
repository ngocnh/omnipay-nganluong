<?php

namespace Omnipay\NganLuong;

use Omnipay\Common\AbstractGateway;

/**
 * NganLuong Pro Class
 */
class ProGateway extends AbstractGateway
{

    public function getName()
    {
        return 'NganLuong Pro';
    }


    public function getDefaultParameters()
    {
        return [
            'username'  => '',
            'password'  => '',
            'signature' => '',
            'testMode'  => false,
        ];
    }


    public function getUsername()
    {
        return $this->getParameter('username');
    }


    public function setUsername($value)
    {
        return $this->setParameter('username', $value);
    }


    public function getPassword()
    {
        return $this->getParameter('password');
    }


    public function setPassword($value)
    {
        return $this->setParameter('password', $value);
    }


    public function getSignature()
    {
        return $this->getParameter('signature');
    }


    public function setSignature($value)
    {
        return $this->setParameter('signature', $value);
    }


    public function authorize(array $parameters = [ ])
    {
        return $this->createRequest('\Omnipay\NganLuong\src\Message\ProAuthorizeRequest', $parameters);
    }


    public function purchase(array $parameters = [ ])
    {
        return $this->createRequest('\Omnipay\NganLuong\src\Message\ProPurchaseRequest', $parameters);
    }


    public function capture(array $parameters = [ ])
    {
        return $this->createRequest('\Omnipay\NganLuong\src\Message\CaptureRequest', $parameters);
    }


    public function refund(array $parameters = [ ])
    {
        return $this->createRequest('\Omnipay\NganLuong\src\Message\RefundRequest', $parameters);
    }
}
