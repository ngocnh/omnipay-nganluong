<?php

namespace Omnipay\NganLuong\Message;

/**
 * PayPal Express Complete Purchase Request
 */
class ExpressCompletePurchaseRequest extends ExpressCompleteAuthorizeRequest
{

    protected $action = 'Sale';
}
